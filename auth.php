<?php

if (isset($_GET['out'])) {
	setcookie('user_id', '');
	require_once('utils.php');
	success();
	return false;
}

function isAuthorized()
{
	if (isset($_COOKIE['user_id'])) {
		return true;
	}
	return false;
}

function showAuth()
{
	?>
<form method="post" action="login.php" class="form-inline pull-left">
    <input type="text" name="login" class="span2" placeholder="Email"/>
    <input type="password" name="password" class="span2" placeholder="Password"/>
    <input class="btn" type="submit" value="Вход"/>
</form>
<div class="span2">
    <a href="#" class="btn-small">Забыли пароль?</a>
    <a href="#" class="btn-small">Регистрация</a>
</div>

<?php
}

function showUser()
{
	?>
<a href="add.php">
    <div class="span4 btn">Добавить раздачу</div>
</a>
<div class="span2 pull-right">
    <b>USER</b>
    <a href="auth.php?out=1">Выйти</a>
</div>


<?php
}

function keepUser()
{
	// если пользователь не авторизован
	if (!isset($_SESSION['user_id'])) {
		// то проверяем его куки
		// вдруг там есть логин и пароль к нашему скрипту

		if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
			// если же такие имеются
			// то пробуем авторизовать пользователя по этим логину и паролю
			$login = mysql_real_escape_string($_COOKIE['login']);
			$password = mysql_real_escape_string($_COOKIE['password']);

			// и по аналогии с авторизацией через форму:

			// делаем запрос к БД
			// и ищем юзера с таким логином и паролем

			$query = "SELECT `id`
                FROM `users`
                WHERE `login`='{$login}' AND `password`='{$password}'
                LIMIT 1";
			$sql = mysql_query($query) or die(mysql_error());

			// если такой пользователь нашелся
			if (mysql_num_rows($sql) == 1) {
				// то мы ставим об этом метку в сессии (допустим мы будем ставить ID пользователя)

				$row = mysql_fetch_assoc($sql);
				$_SESSION['user_id'] = $row['id'];

				// не забываем, что для работы с сессионными данными,
				// у нас в каждом скрипте должно присутствовать session_start();
			} else {
				// только мы не будем давай ссылку на форму авторизации
				// вдруг человек и не хочет был авторизованым
				// а пришел просто поглядеть на наши страницы как гость
			}
		}
	}
}

?>

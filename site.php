<?php
function site($content)
{
	?>
}<!DOCTYPE HTML PUBLIC  "-//IETF//DTD HTML LEVEL 1//EN">
<head lang="ru">
    <TITLE>Blank HTML Level 1 Page</TITLE>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
    <style type="text/css">
        * {
            border: 1px #000000 solid;
        }
    </style>
</head>
<body>
<div class="navbar navbar-inverse">
    <div class="navbar-inner">
        <a class="brand" href="#">Имя проекта</a>
        <ul class="nav">
            <li>
                <a href="#">На главную</a>
            </li>
        </ul>
    </div>
</div>
<div class="container" name="login">
	<?php
	if (isset($_COOKIE['error'])) {
		echo '<div class="span11 alert alert-error">' . $_COOKIE['error'] . '</div>';
	}
	setcookie('error', '');
	?>
    <div class="span7">
		<?php
		require_once("auth.php");
		if (isAuthorized()) {
			showUser();
		} else {
			showAuth();
		}
		?>
    </div>
    <form class="navbar-search pull-right">
        <input type="text" class="span5 search-query" placeholder="Поиск...">
    </form>
</div>
<br>

</div>
<hr>
	<?php
	echo $content();
	?>
</body>
</html>

<?php
}

?>
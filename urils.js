/*
var BB_ROOT       = "./";
var cookieDomain  = ".rutracker.org";
var cookiePath    = "/forum/";
var cookieSecure  = 0;
var cookiePrefix  = "bb_";
var LOGGED_IN     = 1;
var InfoWinParams = 'width=780,height=510,resizable=yes';

var user = {
    opt_js: {"only_new":0,"h_flag":0,"h_av":0,"h_rnk_i":0,"h_post_i":0,"i_aft_l":0,"h_smile":0,"h_sig":0,"sp_op":0,"tr_tm":0,"h_cat":"","h_tsp":0,"hl_brak":1,"div_tag":1,"h_ta":0},

    set: function(opt, val, days, reload) {
        this.opt_js[opt] = val;
        setCookie('opt_js', $.toJSON(this.opt_js), days);
        if (reload) {
            window.location.reload();
        }
    }
}

$(function(){
    $('a.dl-stub').each(function(){
        var $a = $(this);
        var href = $a.attr('href');
        var t_id = href.slice( href.lastIndexOf('=')+1 );
        var event = ($.browser.opera) ? 'mouseover' : 'mousedown';
        $a.bind(event, function(){
            setCookie('bb_dl', t_id, 'SESSION')
        });
        $a.click(function(){
            $('#dl-form').attr('action', href);
            $('#dl-form').submit();
            return false;
        });
    });
    $('form.tokenized').append('<input type="hidden" name="form_token" value="5db1cffc27c133a202bc9e49b38c85b5" />');
    ajax.form_token = '5db1cffc27c133a202bc9e49b38c85b5';
});

var ajax = new Ajax('http://rutracker.org/forum/ajax.php', 'POST', 'json');

function getElText (e)
{
    var t = '';
    if (e.textContent !== undefined) {
        t = e.textContent;
    }
    else if (e.innerText !== undefined) {
        t = e.innerText;
    }
    else {
        t = jQuery(e).text();
    }
    return t;
}
function escHTML (txt) {
    return txt.replace(/</g, '&lt;');
}
function cfm (txt)
{
    return window.confirm(txt);
}
*/
function post2url (url, params) {
    params = params || {};
    params['form_token'] = '5db1cffc27c133a202bc9e49b38c85b5';
    var f = document.createElement('form');
    f.setAttribute('action', url);
    f.setAttribute('method', 'post');
    f.setAttribute('target', params['target'] || '_self');
    for (var k in params) {
        var h = document.createElement('input');
        h.setAttribute('type', 'hidden');
        h.setAttribute('name', k);
        h.setAttribute('value', params[k]);
        f.appendChild(h);
    }
    document.body.appendChild(f);
    f.submit();
    return false;
}
<?php

function error($text, $hold = false)
{
	setcookie("error", $text);
	if (!isset($hold) || $hold = false) {
		header("Location: http://localhost/bootstrap");
	} else {
		header("location: " . $_SERVER['HTTP_REFERER']);
	}
	exit;
}

function success($main = false)
{
	if (!isset($main) || $main == false) {
		header("Location: " . $_SERVER['HTTP_REFERER']);
	} else {
		header("Location: http://localhost/bootstrap");
	}
}

function alertMsg($text)
{
	return '<div class="alert alert-error">' . $text . '</div>';
}

?>
